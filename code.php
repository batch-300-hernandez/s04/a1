<?php

use Building as GlobalBuilding;

class Building {

    public $name;
    public $floors;
    public $address;

    public function __construct($name, $floors, $address){

        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name){
       
        if(gettype($name) === "string") {
            $this->name = $name;
        }
    }

    public function getFloors(){
        return "The $this->name has $this->floors floors.";
    }

    private function setFloors($floors){
       
        if(gettype($floors) === "string") {
            $this->floors = $floors;
        }
    }

    public function getAddress(){
        return "The $this->name is located at $this->address.";
    }

    private function setAddress($address){
       
        if(gettype($address) === "string") {
            $this->address = $address;
        }
    }

};

class Condominium extends Building {

};

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');