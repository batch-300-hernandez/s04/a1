<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04 Access Modifiers and Inheritance - Activity</title>
</head>
<body>

    <h2>Building</h2>
    <p>The name of the building is <?php echo $building->getName(); ?>.</p>
    <p><?php echo $building->getFloors(); ?></p>
    <p><?php echo $building->getAddress(); ?></p>
    <?php $building->setName('Caswynn Complex'); ?>
    <p>The name of the building has been changed to <?php echo $building->getName(); ?>.</p>

    <h2>Condominium</h2>
    <p>The name of the condominium is <?php echo $condominium->getName(); ?>.</p>
    <p><?php echo $condominium->getFloors(); ?></p>
    <p><?php echo $condominium->getAddress(); ?></p>
    <?php $condominium->setName('Enzo Tower'); ?>
    <p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?>.</p>

</body>
</html>